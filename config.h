#include "rule.h"

/* rules to match the target against
 * - type: what to match the pattern against
 * - pattern: extended regex
 * - action: action to perform upon matching
 */
static const Rule rules[] = {
	/* type, pattern, action */
	{Name, "http(s|)://", "librewolf %s"},
	{Mime, "image/.+", "aseprite %s"},
};

