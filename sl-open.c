#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <magic.h>
#include <regex.h>
#include <libnotify/notify.h>

#include "config.h"

/* notification body */
static char body[BUFSIZ];

/* combine the rule's action and target to create a command that is then
 * executed in the background */
int
perform_action(const char *action, const char *target)
{
	char *command = malloc(strlen(action) + strlen(target) - 2);
	sprintf(command, action, target);
	popen(command, "r");
	free(command);
	return 0;
}

void
notify()
{
	fprintf(stderr, "sl-open: %s\n", body);
	NotifyNotification *n = notify_notification_new("sl-open", body, NULL);
	notify_notification_show(n, NULL);
}

int
main(const int argc, char *argv[])
{
	notify_init("sl-open");
	/* make sure only one argument is passed */
	if (argc != 2) {
		sprintf(body, "only takes 1 parameter");
		notify();
		notify_uninit();
		return EXIT_FAILURE;
	}
	/* open magic database */
	magic_t magic = magic_open(MAGIC_MIME_TYPE | MAGIC_CHECK);
	magic_load(magic, NULL);
	/* match patterns */
	regex_t regex;
	for (size_t p = 0; p < sizeof(rules) / sizeof(*rules); p++) {
		/* skip invalid patterns */
		if (regcomp(&regex, rules[p].pattern, REG_EXTENDED)) {
			sprintf(body, "invalid regex %s", rules[p].pattern);
			notify();
			continue;
		}
		/* match based on type */
		const char *match = rules[p].type == Name ?
			argv[1] :
			magic_file(magic, argv[1]);
		if (!regexec(&regex, match, 0, NULL, 0)) {
			regfree(&regex);
			magic_close(magic);
			notify_uninit();
			/* perform action */
			return perform_action(rules[p].action, argv[1]);
		}
		regfree(&regex);
	}
	magic_close(magic);
	sprintf(body, "no pattern to match %s", argv[1]);
	notify();
	notify_uninit();
	return EXIT_FAILURE;
}
