#ifndef RULE_H
#define RULE_H

typedef enum {
	Name,
	Mime,
} RuleType;

typedef struct {
	const RuleType type;
	const char *pattern;
	const char *action;
} Rule;

#endif
