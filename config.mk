# paths
PREFIX   := /usr/local
BINDIR   := ${DESTDIR}${PREFIX}/bin
# flags
CFLAGS   := -g -std=c99 -pedantic -Wall -O0 # development
#CFLAGS   := -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Os # release
CPPFLAGS := -D_DEFAULT_SOURCE -D_POSIX_C_SOURCE=200809L
LDFLAGS  := -lmagic
# notification
CFLAGS   += `pkgconf --cflags libnotify`
LDFLAGS  += `pkgconf --libs libnotify`
# compiler
CC       := cc
