# sl-open - suckless xdg-open

## features

- suckless
- extended regex
- MIME type
- notifications

## requirements

- c99
- libmagic
- libnotify

## installation

- edit `config.mk` if needed
- run the following command to build and install sl-open (as root if necessary)
```
make clean install
```
- *Optional*: run the following to replace `xdg-open`
```
ln -sf sl-open /bin/xdg-open
```

## configuration

sl-open is configured through config.h

## alternatives

- [xdg-open](https://cgit.freedesktop.org/xdg/xdg-utils/tree/scripts/xdg-open.in)
  `SHELL` 579LOC original, bloated
- [sx-open](https://code.fleshless.org/fbt/sx-open) `BASH` 265LOC
- [tao](https://github.com/wooosh/tao) `C` 173LOC no regex
- [soap](https://github.com/FRIGN/soap) `C` 56LOC no mime
