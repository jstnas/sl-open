.POSIX:

include config.mk

TARGET := sl-open

${TARGET}: ${TARGET}.o

${TARGET}.o: ${TARGET}.c config.h rule.h

clean:
	rm -f ${TARGET} *.o

install: ${TARGET}
	mkdir -p ${BINDIR}
	cp -f ${TARGET} ${BINDIR}
	chmod 755 ${BINDIR}/${TARGET}

uninstall:
	rm -f ${BINDIR}/${TARGET}

.PHONY: clean install uninstall
